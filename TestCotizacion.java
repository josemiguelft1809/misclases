/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;
import java.util.Scanner;
/**
 *
 * @author Jose Figueroa
 */
public class TestCotizacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // TODO code application logic here
        Cotizacion cotizacion = new Cotizacion();
        Scanner sc =new Scanner (System.in);
        int opc = 0, numCotizacion = 0, plazo = 0;
        float precio = 0.0f, porcentajePagoInicial = 0.0f;
        String descripcion = null;
        
        do{
            System.out.println("1- Iniciar el objeto");
            System.out.println("2- Cambiar Numero de Cotizacion");
            System.out.println("3- Cambiar Descripcion");
            System.out.println("4- Cambiar Precio");
            System.out.println("5- Cambiar Porcentaje");
            System.out.println("6- Cambiar Plazo");
            System.out.println("7- Mostrar Informacion");
            System.out.println("8- Salir");
            System.out.print("Dame la opcion: ");
            opc = sc.nextInt();
            
            switch(opc){
                case 1:
                    System.out.print("Digite el numero de cotizacion: ");
                    numCotizacion = sc.nextInt();
                    sc.nextLine();
                    System.out.print("Digite la descripcion del automovil: ");
                    descripcion = sc.nextLine();
                    System.out.print("Digite el precio: ");
                    precio = sc.nextFloat();
                    System.out.print("Dijite el porcentaje del pago inicial: ");
                    porcentajePagoInicial = sc.nextFloat();
                    System.out.print("Digite el plazo de pago: ");
                    plazo = sc.nextInt();
                    cotizacion.setNumCotizacion(numCotizacion);
                    cotizacion.setDescripcionAutomovil(descripcion);
                    cotizacion.setPrecio(precio);
                    cotizacion.setPorcentajePagoInicial(porcentajePagoInicial);
                    cotizacion.setPlazo(plazo);
                    break;
                case 2:
                    System.out.print("Digite el numero de cotizacion: ");
                    numCotizacion = sc.nextInt();
                    cotizacion.setNumCotizacion(numCotizacion);
                    break;
                case 3:
                    System.out.print("Digite la descripcion del automovil: ");
                    sc.nextLine();
                    descripcion = sc.nextLine();
                    cotizacion.setDescripcionAutomovil(descripcion);
                    break;
                case 4:
                    System.out.print("Digite el precio: ");
                    precio = sc.nextFloat();
                    cotizacion.setPrecio(precio);
                    break;
                case 5:
                    System.out.print("Dijite el porcentaje del pago inicial: ");
                    porcentajePagoInicial = sc.nextFloat();
                    cotizacion.setPorcentajePagoInicial(porcentajePagoInicial);
                    break;
                case 6:
                    System.out.print("Digite el plazo de pago: ");
                    plazo = sc.nextInt();
                    cotizacion.setPlazo(plazo);
                    break;
                case 7:
                    cotizacion.imprimirCotizacion();
                    break;
                case 8:
                    System.out.println("Salio con exito");
                    break;
                default:
                    System.out.println("Opcion invalida");
            }
        } while (8!=opc);
    }
}